import tkinter as tk
import tkinter.filedialog
import random

title_label = None
first_line_label = None
second_line_label = None
third_line_label = None

def check_unique(words):
    if len(words) != len(set(words)):
        return False
    return True

def generate_random_word(nouns,verbs,adjectives,prepositions,adverb,frame3):
    nouns_value = nouns.get().split(",")
    verbs_value = verbs.get().split(",")
    adjectives_value = adjectives.get().split(",")
    prepositions_value = prepositions.get().split(",")
    adverb_value = adverb.get().split(",")

    if len(nouns_value) < 3:
        ent_nouns.config(bg='red')
        tk.messagebox.showerror("Error", "Enter at least 3 nouns.")
        return
    if len(verbs_value) < 3:
        ent_verbs.config(bg='red')
        tk.messagebox.showerror("Error", "Enter at least 3 verbs.")
        return
    if len(adjectives_value) < 3:
        ent_adjectives.config(bg='red')
        tk.messagebox.showerror("Error", "Enter at least 3 adjectives.")
        return
    if len(prepositions_value) < 2:
        ent_prepositions.config(bg='red')
        tk.messagebox.showerror("Error", "Enter at least 2 prepositions.")
        return
    if len(adverb_value) < 1:
        ent_adverb.config(bg='red')
        tk.messagebox.showerror("Error", "Enter at least 1 adverb.")
        return

    if not check_unique(nouns_value):
        tk.messagebox.showerror("Error", "Enter unique nouns.")
        return
    if not check_unique(verbs_value):
        tk.messagebox.showerror("Error", "Enter unique verbs.")
        return
    if not check_unique(adjectives_value):
        tk.messagebox.showerror("Error", "Enter unique adjectives.")
        return
    if not check_unique(prepositions_value):
        tk.messagebox.showerror("Error", "Enter unique prepositions.")
        return
    if not check_unique(adverb_value):
        tk.messagebox.showerror("Error", "Enter unique adverbs.")
        return


    random_nouns = random.choices(nouns_value,k = 3)
    random_verbs = random.choices(verbs_value,k = 3)
    random_adjectives = random.choices(adjectives_value,k = 3)
    random_prepositions = random.choices(prepositions_value,k = 2)
    random_adverb = random.choice(adverb_value)
    vowels = "aeioy"
    entry = "A"
    for i in range(0, len(vowels)):
        if(random_adjectives[0].lower() == vowels[i]):
            entry = "An"

    global title_label, first_line_label, second_line_label, third_line_label
    title_label = tk.Label(frame3, text=f"{entry} {random_adjectives[0]} {random_nouns[0]}", pady=30,bg="lightgrey")
    title_label.pack()
    first_line_label = tk.Label(frame3, text=f"{entry.lower()} {random_adjectives[0]} {random_nouns[0]} {random_verbs[0]} {random_prepositions[0]} the {random_adjectives[1]} {random_nouns[1]}", pady=3,bg="lightgrey")
    first_line_label.pack()
    second_line_label = tk.Label(frame3, text=f"{random_adverb} the {random_nouns[1]} {random_verbs[1]}", pady=3,bg="lightgrey")
    second_line_label.pack()
    third_line_label = tk.Label(frame3, text=f"the {random_nouns[1]} {random_verbs[2]} {random_prepositions[1]} a {random_adjectives[2]} {random_nouns[2]}", pady=3,bg="lightgrey")
    third_line_label.pack()
    save_button = tk.Button(frame3, text="Save to file", command=save_to_file)
    save_button.pack(pady=20)

def save_to_file():
    labels_text = [title_label.cget("text"), first_line_label.cget("text"), second_line_label.cget("text"), third_line_label.cget("text")]

    file_path = tkinter.filedialog.asksaveasfilename(defaultextension=".txt")

    if file_path:
        with open(file_path, "w") as file:
            for line in labels_text:
                file.write(line + "\n")

root = tk.Tk()
root.title("My Own Poem")
root.geometry("800x800")


frame1 = tk.Frame(root)
frame1_height = 50
frame1.pack(side=tk.TOP, fill=tk.X)
frame1.config(height=frame1_height)

label_center = tk.Label(frame1, text="Enter your favorite words, separated by commas", pady=10)
label_center.pack()


frame2 = tk.Frame(root)
frame2.pack(side=tk.TOP, fill=tk.X)


frame2.grid_columnconfigure(0, weight=1)
frame2.grid_columnconfigure(1, weight=4)


nouns = tk.StringVar()
verbs = tk.StringVar()
adjectives = tk.StringVar()
prepositions = tk.StringVar()
adverb = tk.StringVar()

lbl_nouns = tk.Label(frame2, text="Nouns")
ent_nouns = tk.Entry(frame2, textvariable=nouns)
lbl_nouns.grid(row=0, column=0, padx=10, pady=5, sticky="e")
ent_nouns.grid(row=0, column=1, padx=10, pady=5, sticky="ew")
nouns.set("At least 3 nouns")

lbl_verbs = tk.Label(frame2, text="Verbs")
ent_verbs = tk.Entry(frame2, textvariable=verbs)
lbl_verbs.grid(row=1, column=0, padx=10, pady=5, sticky="e")
ent_verbs.grid(row=1, column=1, padx=10, pady=5, sticky="ew")
verbs.set("At least 3 verbs")

lbl_adjectives = tk.Label(frame2, text="Adjectives")
ent_adjectives = tk.Entry(frame2, textvariable=adjectives)
lbl_adjectives.grid(row=2, column=0, padx=10, pady=5, sticky="e")
ent_adjectives.grid(row=2, column=1, padx=10, pady=5, sticky="ew")
adjectives.set("At least 3 adjectives")

lbl_prepositions = tk.Label(frame2, text="Prepositions")
ent_prepositions = tk.Entry(frame2, textvariable=prepositions)
lbl_prepositions.grid(row=3, column=0, padx=10, pady=5, sticky="e")
ent_prepositions.grid(row=3, column=1, padx=10, pady=5, sticky="ew")
prepositions.set("At least 3 prepositions")

lbl_adverb = tk.Label(frame2, text="Adverb")
ent_adverb = tk.Entry(frame2, textvariable=adverb)
lbl_adverb.grid(row=4, column=0, padx=10, pady=5, sticky="e")
ent_adverb.grid(row=4, column=1, padx=10, pady=5, sticky="ew")
adverb.set("At least 1 adverb")


button_generate = tk.Button(root, text="Generate", command=lambda: generate_random_word(nouns, verbs, adjectives, prepositions, adverb, frame3))
button_generate.pack(pady=20)

frame3_height = 800 - frame1_height - frame2.winfo_reqheight()
frame3 = tk.Frame(root, bg="lightgrey", relief="groove", borderwidth=5)
frame3.pack(side=tk.TOP, fill=tk.BOTH, expand=True, padx=10, pady=10)
frame3.config(height=frame3_height)


your_poem_label = tk.Label(frame3, text="Your poem:", pady=30, bg="lightgrey")
your_poem_label.pack()


root.mainloop()
